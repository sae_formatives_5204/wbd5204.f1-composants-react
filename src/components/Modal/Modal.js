import "./modal.scss";

export default ({setVisibleModal,visible, title, children}) => {


    function onClose() {
        setVisibleModal(false);
    }

    function onOk() {
        alert("ok")
    }

    if (!visible) return true;

    return (
        <div className="modalContainer">
            <div className="modalContent">
                <h1>{title}</h1>
                <hr/>
                {children}
                <div className="modalButtons">
                    <button onClick={onClose} className="button is-danger">Fermer</button>
                    <button onClick={onOk} className="button is-primary">Ok</button>
                </div>
            </div>
        </div>

    );
}