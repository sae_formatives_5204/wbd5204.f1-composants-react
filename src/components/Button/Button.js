import {useEffect, useState} from "react";

export default ({name,disabled,type,loading,onClick}) => {

    const [isDisabled,setIsDisabled] = useState(disabled);

    useEffect(() => {
        if (!disabled && loading) setIsDisabled(true);
    },[])

    return(
      <button
          className={`button is-${type}`}
          disabled={isDisabled}
          onClick={onClick}
      >
          {name}
      </button>
    );
}