import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import { useState } from "react";

export default function App() {
    const [visibleModal,setVisibleModal] = useState(false);

    function openModal(){
        setVisibleModal(true);
    }

    return (
        <>
            {/* TITLE */}
            <h1>WBD5204.F1 - Composants React</h1>

            {/* BUTTON*/}
            <Button onClick={openModal} name="Modal" type="primary" disabled={false} loading={false}/>

            {/* MODAL */}
            <Modal title="Modal test" setVisibleModal={setVisibleModal} visible={visibleModal}>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris at ex at arcu rhoncus vulputate nec
                    ut turpis. Sed aliquam tortor quis odio interdum, ac laoreet ipsum euismod. Nullam consectetur ipsum
                    id viverra iaculis. Fusce suscipit finibus risus vel vestibulum. Pellentesque vitae purus felis. In
                    tempor diam quis mauris bibendum hendrerit. Proin enim sapien, consectetur et mauris id, posuere
                    gravida odio. Maecenas sed auctor orci, sit amet vestibulum tortor. Integer faucibus felis eget
                    dolor euismod accumsan. In eget imperdiet ipsum, et ullamcorper nulla. Phasellus tempor, erat et
                    auctor aliquam, diam neque sagittis sapien, lobortis volutpat elit nibh id ligula. Ut diam neque,
                    vehicula ut erat nec, ultricies mattis lectus. Mauris eget ligula elit. Suspendisse eget est ut nisi
                    dapibus condimentum. Mauris efficitur sed nulla vitae ornare.
                </p>
            </Modal>
        </>
    );
}
